angular.module('psnit-app.services', ['ngResource'])

/**
 * Cache factory
 */
.factory('appCache', function($cacheFactory) {
    return $cacheFactory('appData');
})

/**
 * Global user search settings
 */
.factory('searchSettings', function(){
    var options = {
        showOpen: false,
        showActive: false,
    }
    return options;
})

/**
 * Stores info for data update
 */
.factory('updateData', function(){
    'use strict';

    var state = false;
    return {
      state: state,
    };
})


/**
 * Stores config data
 */
.factory('config', function($scope) {
    return $cacheFactory('appData');
})

/**
 * Stores user Faverite Station List
 */
.factory('faveriteService', ['myDatabase', function(myDatabase) {
    return {
        insert: function(station) {
            station.faverite = true;
            myDatabase.addFaverite(station.sid);
        },
        remove: function(station) {
            station.faverite = false;
            myDatabase.removeFaverite(station.sid);
        }
    }
}])

/**
 * SQL Liste database service stored in local app
 */
.factory('myDatabase', ['$cordovaSQLite', '$q',
    function($cordovaSQLite, $q) {
        return {
            addFaverite: function(newSid) {
                var query = "INSERT INTO Favorites (sid) VALUES (?)";
                $cordovaSQLite.execute(db, query, [newSid]).then(function(res) {
                    console.log("INSERT ID -> " + res.insertId);
                }, function(err) {
                    console.error(err);
                });
            },
            removeFaverite: function(sid) {
                var query = "DELETE FROM Favorites WHERE sid == ?";
                $cordovaSQLite.execute(db, query, [sid]).then(function(res) {
                    console.log("Removed ID");
                }, function(err) {
                    console.error(err);
                });
            },
            getFaveriteList: function(data) {

                var query = "SELECT sid FROM Favorites";
                var deferred = $q.defer();
                if (ionic.Platform.isIOS() || ionic.Platform.isAndroid()) {
                    // mobile app query
                    $cordovaSQLite.execute(db, query).then(function(res) {
                            data.favoriteNo = 0;

                            angular.forEach(data, function(station, key) {
                                for (i = 0; i < res.rows.length; i++) {
                                    if (station.sid == res.rows.item(i).sid) {
                                        station.faverite = true;
                                        data.favoriteNo = i + 1;
                                    }
                                }
                            })
                            return deferred.resolve(res);
                        }),
                        function(err) {
                            console.error(err);
                        }
                } else {
                    // web browser qery 
                    $cordovaSQLite.execute(db, query).then(function(res) {
                        data.favoriteNo = 0;
                        angular.forEach(data, function(station, key) {

                            angular.forEach(res.rows, function(fav, favKey) {
                                if (station.sid == fav.sid) {
                                    station.faverite = true;
                                    data.favoriteNo++;
                                }
                            })
                        })
                        return deferred.resolve(res);
                    }, function(err) {
                        console.error(err);
                    });
                }
                return deferred.promise;
            }
        }
    }
])

/**
 * SQL Liste database service stored in local app
 */
.factory('getBaseURL', ['md5', '$filter',
    function(md5, $filter) {

        // stores url parameters
        var urlMD5Key = md5.createHash('--' + $filter('date')(new Date(), 'yyyy-MM-dd') + '--iOS');
        var myURL = "http://00.00.00.00/index.php?/stations/api/&key=" + urlMD5Key + "&lang=pl_PL";

        return {
            urlStations: function() {
                return myURL + '?cmd=stations';
            },
            urlRegions: function() {
                return myURL + '?cmd=regions';
            },
            urlImage: function() {
                return myURL + '?cmd=image';
            },
            url: function() {
                return myURL;
            }
        }
    }
])

/**
 * GPS location service
 */
.factory('GpsPosition', ['$cordovaGeolocation', '$q', 'appCache', '$ionicPopup',
    function($cordovaGeolocation, $q, appCache, $ionicPopup) {

        var posOptions = {
            timeout: 8000,
            enableHighAccuracy: true
        };

        return {
            getCurrentLocation: function() {
                var deferred = $q.defer();
                var cachedData = appCache.get('deviceLocation');

                if (!cachedData) {
                    $cordovaGeolocation
                        .getCurrentPosition(posOptions)
                        .then(function(position) {
                            appCache.put('deviceLocation', position);

                            deferred.resolve(position);
                        },function(error){                                                        
                             // A confirm dialog  redirecting to settings panel
                            if ( ionic.Platform.isAndroid() ) {
                               var confirmPopup = $ionicPopup.confirm({
                                 title: 'Nie można odczytać pozycji',
                                 template: 'Czy przejść do ustawień ?'
                               });

                               confirmPopup.then(function(res) {
                                 if(res) {
                                   window.cordova.plugins.diagnostic.switchToLocationSettings();
                                 } else {
                                   appCache.put('deviceLocation', null);
                                   deferred.resolve();
                                 }
                               });
                           }
                            // Info for IOS
                            else if( ionic.Platform.isIOS() ) {
                                alert('Nie można odczytać pozycji, sprawdź ustawienia GPS'); 
                            }
                            // Info for other devices
                            else{
                                alert('Nie można odczytać pozycji, sprawdź ustawienia GPS'); 
                            }

                        })
                } else {
                    deferred.resolve(cachedData);
                }
                return deferred.promise;
            },
            getDistance: function(stations, location) {
                // Count distance between device location and given gps coorinates
                var array =[]
                var R = 6371; // Radius of the earth in km
                angular.forEach(stations, function(station, key) {

                    var dLat = deg2rad(location.coords.latitude - station.latitude); // deg2rad below
                    var dLon = deg2rad(location.coords.longitude - station.longtitude);

                    var a =
                        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                        Math.cos(deg2rad(location.coords.latitude)) * Math.cos(deg2rad(station.latitude)) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                    var d = R * c; // Distance in km

                    var distance = Math.round(d);

                    station.distance = distance;

                    array.push(station);
                })

                return array;

                function deg2rad(deg) {
                    return deg * (Math.PI / 180)
                }

            }
        }
    }
])

/**
 * API service for loading station data
 */
.factory('ApiService', ['$http', '$q', '$cacheFactory', 'getBaseURL', 'GpsPosition', 'appCache', 'myDatabase', '$filter',
    function($http, $q, $cacheFactory, getBaseURL, GpsPosition, appCache, myDatabase, $filter) {
        return {
            getAll: function() {

                var getData = $http.jsonp(getBaseURL.urlStations(), {
                    params: {
                        cache: false,
                        'callback': 'JSON_CALLBACK'
                    }
                });

                var deferred = $q.defer();
                var cachedData = appCache.get('stationsData');

                if (!cachedData) {
                    getData.success(function(data) {

                        myDatabase.getFaveriteList(data).then(
                            function(data) {
                            }
                        );
                        appCache.put('stationsData', data);
                        deferred.resolve(data);
                    }).error(function(data, status, headers, config) {
                        deferred.reject();
                    });
                } else {
                    deferred.resolve(cachedData);
                }
                return deferred.promise;
            },
            getFavoriteList: function(){

                var getData = $http.jsonp(getBaseURL.urlStations(), {
                    params: {
                        cache: false,
                        'callback': 'JSON_CALLBACK'
                    }
                });

                var deferred = $q.defer();
                var cachedData = appCache.get('stationsData');

                    getData.success(function(data) {

                        myDatabase.getFaveriteList(data).then(
                            function(data) {

                            }
                        );
                       // appCache.put('stationsData', data);
                       deferred.resolve(data);
                    }).error(function(data, status, headers, config) {
                        deferred.reject();
                    });

                return deferred.promise;
            },
            getRegionList: function(){
                var deferred = $q.defer();
                var cachedData = appCache.get('regionsData');

                if (!cachedData) {
                    var getRegions = $http.jsonp(getBaseURL.urlRegions(), {
                        cache: false,
                        params: {
                            'callback': 'JSON_CALLBACK'
                        }
                    });

                    getRegions.success(function(data) {
                        deferred.resolve(data);
                    })
                } else {
                    deferred.resolve(cachedData);
                }
                return deferred.promise;
            },
            getStationDetails: function(stationId) {
                var deferred = $q.defer();
                var stationsArray = appCache.get('stationsData');


                for (var key in stationsArray) {
                    if (stationsArray[key].sid == stationId) {
                        deferred.resolve(stationsArray[key])
                    }
                }

                return deferred.promise;
            },
            getMoreInfo: function(stationId, type) {
                var deferred = $q.defer();

                var getPricing = $http.jsonp(getBaseURL.url() + '?cmd=' + type + '/' + stationId, {
                    cache: false,
                    params: {
                        'callback': 'JSON_CALLBACK'
                    }
                });

                console.log(getBaseURL.url() + '?cmd=' + type + '/' + stationId)

                getPricing.success(function(data) {
                    deferred.resolve(data['result'])
                })

                return deferred.promise;
            },
            getNews: function(stationId) {
                var deferred = $q.defer();

                var getServices = $http.jsonp(getBaseURL.urlServices() + '/' + stationId, {
                    cache: false,
                    params: {
                        'callback': 'JSON_CALLBACK'
                    }
                });

                getServices.success(function(data) {
                    deferred.resolve(data['result']);
                })

                return deferred.promise;
            },
            sendVote: function(deviceId, stationId, voteValue) {
                var deferred = $q.defer();
                var sendVote = $http.jsonp(getBaseURL.url() + '?cmd=vote/' + deviceId + '/' + stationId + '/' + voteValue, {
                    cache: false,
                    params: {
                        'callback': 'JSON_CALLBACK'
                    }
                });

                sendVote.success(function(data) {
                    deferred.resolve(data);
                })

                return deferred.promise;

            },
            getVote: function(deviceId, stationId) {
                var deferred = $q.defer(); 

                var getVote = $http.jsonp(getBaseURL.url() + '?cmd=ratings/' + deviceId, {
                    cache: false,
                    params: {
                        'callback': 'JSON_CALLBACK'
                    }
                });

                getVote.success(function(data) {
                    stationVote = $filter('filter')(data, function(d) {
                        return d.sid == stationId;
                    })[0];

                    deferred.resolve(stationVote);
                }).error(function(data, status, headers, config) {
                    console.log('nie udalo sie wczytac glosow')
                    deferred.reject();
                });

                return deferred.promise;
            }
        };
    }
]);
