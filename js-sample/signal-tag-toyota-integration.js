 /**
   * Toyota signal integration script
   *
   * It is required for signal tag integration with google analitycs
   * 
   * -------------------- ! important -------------------
   * attribute data-siganltag-category is required 
   *
   * How to use:
    <a href="#" data-siganltag-category="hybrid.sites.toyota.pl" 
                data-siganltag-action="boczne_menu"
                data-siganltag-value="znajdz_dilera" 
    > some text </a>

   *  
   *
   * @description It is used for It is required for operation of the signal tag
   * @author Dawid Wolosz <dawo@closbrothers.pl>
   * @version 1.1.2
   * @date 02.11.2015
   */

 function SignalTagRun() {
     // enabled development mode with comments
     var devMode = true;
     if (devMode) {
         // wait some time to check if signal tag script is initialized properly
         setTimeout(
             function() {
                 if (window.t1DataLayer) {
                     console.info('[ SignalTagRun initialized ]');
                     // displays all signal tag links if they exists
                     if ($('*[data-siganltag-category]')[0]) {
                         console.info('[links found] ----------------------------');
                         $('*[data-siganltag-category]').each(function(index, value) {
                             console.log(value);
                         });
                         console.info('[end of founded links list ] ----------------------------');
                     } else {
                         console.info('[ ...no signal tag links found ... ]')
                     }
                 } else {
                     console.error('[ SignalTagRun ininitlization failed. Please check if signal tag script is included properly ]');
                 }
             }, 500);
     }

     /**
      * Init post message
      */
     this.init = function() {
         $('*[data-siganltag-category]').on("click tap", function(event) {
             $clickedItem = $(this);

             //assigning data 
             sendCategory = $clickedItem.data('siganltag-category'); // display in events google category
             sendAction = $clickedItem.data('siganltag-action'); // display in events google action
             sendValue = $clickedItem.data('siganltag-value'); // display in events google label

             // default values if empty
             if (!sendAction) {
                 sendAction = null;
             }

             if (!sendValue) {
                 sendValue = null;
             }

             // wraping data into event
             window.t1DataLayer.event = {
                 componentname: sendCategory, // display in google category
                 action: sendAction, // display in google action
                 value: sendValue, // display google label
                 category: sendCategory, // signal tag category
             };

             /*-----------------------------------------
                 Vanilla JavaScript event send (IE9+)
             -----------------------------------------*/
             var event;
             if (window.CustomEvent) {
                 event = new CustomEvent('ctaevent');
             } else {
                 event = document.createEvent('CustomEvent');
                 event.initCustomEvent('ctaevent', true, true, {});
             }
             window.dispatchEvent(event);

             if (devMode) {
                 console.log('send event:');
                 console.log(event);
                 console.log(window.t1DataLayer.event);
             }

         });
     }
 }

 var t1signalTag = new SignalTagRun();
 t1signalTag.init();
